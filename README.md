A simple URL Shortner written in Node.js

Set MONGODB_URL and BASE_URL in .env file of root folder
Run "npm i"
Run test Cases using "npm test"
Run application using "npm start"

api's
1.  method:POST

    url: /short 
    
    body: {
        url:'http://www.google.com'
    }
   

2.   method:GET

        url: /:shortUrl
       
   
